<?php

namespace Clases;

/**
 * @description     Programmer information.
 * @author          Burno Paglialunga brunopaglialunga@gmail.com
 */
class Programmer extends Employee
{
    private $language = null;
    static $_languages = array('PHP', 'Python', 'NET');

    function __construct($id = null, $first_name = '', $last_name = '', $age = null, $language = null) {
        if (in_array($language, self::$_languages)){
            parent::__construct($id, $first_name, $last_name, $age);
            $this->language = $language;
        } else {
            throw new Exception('Unsupported language.');
        }
    }

    /**
     * @description  Set language
     * @param       string $language language
     */
    public function setLanguage($language) {
        $this->language = $language;
    }

    /**
     * @description Get language
     * @return      string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @description Get language
     * @return      string
     */
    public function getType() {
        return $this->language;
    }
}
