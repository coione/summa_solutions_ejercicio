<?php

namespace Clases;

/**
 * @description     Designer information.
 * @author          Burno Paglialunga brunopaglialunga@gmail.com
 */
class Designer extends Employee
{
    private $type = null;
    static $_types = array('Gráfico', 'Web');
    
    function __construct($id = null, $first_name = '', $last_name = '', $age = null, $type = null) {
        if (in_array($type, self::$_types)){
            parent::__construct($id, $first_name, $last_name, $age);
            $this->type = $type;
        } else {
            throw new Exception('Unsupported type.');
        }
    }
    
    /**
     * @description Set type
     * @param       string $type type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @description Get type
     * @return      string type
     */
    public function getType() {
        return $this->type;
    }
}
