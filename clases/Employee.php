<?php

namespace Clases;

/**
 * Employee
 * 
 * @description     Information about employee.
 * @author          Burno Paglialunga brunopaglialunga@gmail.com
 */
class Employee
{
    private $id;
    private $first_name = '';
    private $last_name = '';
    private $age = null;

    function __construct($id = null, $first_name = '', $last_name = '', $age = null) {
        if (!is_null($id) && (int) $id !== 0){
            $this->id = (int) $id;
            $this->first_name = $first_name;
            $this->last_name = $last_name;
            $this->age = $age;
        } else {
            throw new \Exception('Invalid Id');
        }
    }

    /**
     * Set id
     *
     * @param int $id employee id
     */
    public function setId($id){
        $this->id = (int) $id;
    }

    /**
     * Set first name
     *
     * @param string $first_name employee first name
     */
    public function setFirstName($first_name){
        $this->first_name = $first_name;
    }

    /**
     * Set last name
     *
     * @param string $last_name employee last name
     */
    public function setLastName($last_name){
        $this->last_name = $last_name;
    }

    /**
     * Set age
     *
     * @param int $age employee age
     */
    public function setAge($age){
        $this->age = $age;
    }

    /**
     * get id
     * 
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * get first name
     * 
     * @return string
     */
    public function getFirstName() {
        return $this->first_name;
    }

    /**
     * get last name
     * 
     * @return string
     */
    public function getLastName() {
        return $this->last_name;
    }

    /**
     * get Age
     * 
     * @return int
     */
    public function getAge() {
        return $this->age;
    }

    public function getProfession(){
        /**
        * @description Return classname
        * @return      string
        */
       
       return str_replace('Clases\\', "", get_class($this));
    }
}