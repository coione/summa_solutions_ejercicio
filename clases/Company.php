<?php

namespace Clases;

/**
 * @description     Company - Information about company and employees.
 * @author          Burno Paglialunga brunopaglialunga@gmail.com
 */
class Company
{
    private $id;
    private $name;
    private $employees = [];

    function __construct($id = null, $name = ''){
        $this->id = (int) $id;
        $this->name = $name;
    }

    /**
     * Set id
     *
     * @param int $id company id
     */
    public function setId($id){
        $this->id = (int) $id;
    }

    /**
     * Set id
     *
     * @param string $name company name
     */
    public function setName($name){
        $this->id = $name;
    }

    /**
     * Insert employee
     *
     * @return bool
     */
    public function setEmployee(Employee $employee){
        if (!is_null($employee) && !$this->getEmployee($employee->getId())){
            $this->employees[] = $employee;
            return true;
        }
        return false;
    }

    /**
     * Company id
     * 
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Company name
     * 
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * get employees
     * 
     * @return array
     */
    public function getEmployees(){
        return $this->employees;
    }

    /**
     * get employee
     * 
     * @param int $id employee id
     * @return obj Employee
     */
    public function getEmployee($id = null){
        if (!is_null($id)) {
            foreach($this->employees as $employee){
                if ($employee->getId() === $id){
                    return $employee;
                }
            }
        }
    }

    /**
     * Average age of employees
     * 
     * @return int
     */
    public function getAverageAge(){
        $total = 0;
        
        foreach($this->employees as $employee){
            $total += (int) $employee->getAge();
        }
        return (count($this->employees) !== 0)? round(($total/count($this->employees))): 0;
    }

    public function viewHeader(){
        $return = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
        $return .= ' Company:  '."\n\n   ".$this->getName()."\n";
        return $return;
    }

    public function viewList(){
        $return = "\n\n";
        $return .= " Employees list:\n\n";
        $mask = " %3.3s |%-30.30s |%4.4s |%15.15s|%15.15s\n";
        
        $return .= sprintf($mask, '#', 'Name', 'Age', 'Profession', 'Language/Type');
        foreach ($this->getEmployees() as $employee) {
            $return .= sprintf(
                    $mask, 
                    $employee->getId(), 
                    $employee->getFirstName().', '.$employee->getLastName(), 
                    is_null($age = $employee->getAge())? '-': $age,
                    $employee->getProfession(),
                    $employee->getType()
                    );
        }
        
        return $return;
    }

    public function viewEmployeeByid($id){
        $return = "\n\n";
        $return .= " Search Employee by Id (id=$id)\n\n";
        $employee = $this->getEmployee($id);
        if(!is_null($employee)){
            $mask = " %3.3s |%-30.30s |%4.4s |%15.15s|%15.15s\n";

            $return .= sprintf($mask, '#', 'Name', 'Age', 'Profession', 'Language/Type');
            $return .= sprintf(
                    $mask, 
                    $employee->getId(), 
                    $employee->getFirstName().', '.$employee->getLastName(), 
                    is_null($age = $employee->getAge())? '-': $age,
                    $employee->getProfession(),
                    $employee->getType()
                    );
        } else {    
            $return .= '   Employee not exist'."\n";
        }

        return $return;
    }

    public function viewAgeAverage(){
        $return = "\n\n";
        $return .= " Age average: ".$this->getAverageAge()."\n"."\n";
        return $return;
    }
}