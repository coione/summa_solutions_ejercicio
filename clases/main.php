<?php
/**
 * Autoloader
 */
function __autoload($className) {
    $parts = explode('\\', $className);
    include end($parts).'.php';
}