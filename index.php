<?php
require __DIR__.'/clases/main.php';

use Clases\Company as Company;
use Clases\Designer as Designer;
use Clases\Programmer as Programmer;


$company = new Company(1, 'Hooli');

$employees = [];
$employees[] = new Designer(1, 'Pablo', 'Picasso', 40, 'Gráfico');
$employees[] = new Programmer(2, 'Steve', 'Wozniak', 67, 'Python');
$employees[] = new Designer(3, 'Andy', 'Warhol', 32, 'Web');
$employees[] = new Programmer(4, 'Rasmus', 'Lerdorf', 47, 'PHP');


foreach($employees as $employee){
    $company->setEmployee($employee);
}

// Header
echo $company->viewHeader();

// Employee List
echo $company->viewList();

// Search employee by random id
$idSearch = rand(0, count($company->getEmployees()));
echo $company->viewEmployeeById($idSearch);

// Age Average
echo $company->viewAgeAverage();

